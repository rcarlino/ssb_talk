### Secure Scuttlebutt Protocol

Replicated, "off grid" log database for P2P apps

---

### Outline

 * Introduce an SSB application, Pathwork
 * Cover the basics of the protocol's architecture.
 * Hand it over to the SSB devs

---

### How I Got Involved

 * My project needed data replication over unreliable connections.
 * I needed a database that can tolerate partitions and gain consistency later.
 * Was not feasible for our project, but...
   * Learned an amazing protocol
   * Found a fun P2P social app (Patchwork)

---

### Real SSB Use Case: Patchwork

 * SSB is just a database.
 * Applications utilize Databases.
 * Patchwork is a P2P social messenger (think Twitter), powered by SSB
 * (intermission) Patchwork test drive.

---

### How SSB Makes Patchwork Different

  * End-to-end encryption
  * Internet not required, even for posting messages
  * Offline first, sync later

---

### So What is SSB Then?

  * The protocol powering Patchwork
  * Usually used for offline, peer to peer social apps, similar to Twitter.
    * It is not exclusively a social app technology (SEE: `ssb-chess`).
    * The protocol lends itself well to social apps.
  * Why the name?

---

### Key Concepts

  * No centralized authorities, ever (Later: Dark Crystal)
    * Avoid singletons (Servers, DHTs, Global blockchains)
  * "Free Listening" - ability to refuse peer messages.
  * "Gossip" and "Listening Distance" - Hops between peers

---

### How Patchwork Works

 * When you follow a user, you will:
   * Replicate logs of that friend
   * Replicate that user's friends, also (configurable)
 * Once connected you can
   * send end-to-end private messages
   * Change clients (patchbay, patchfoo, patchwork)
   * Block users (stop replicating)
   * Name users ("pet names")

---

### First Use

 * Updates are propagated via "gossip" with online peers
 * Only see your own posts (no peers, just a diary)
 * SSB server searches LAN for peers via UDP (if any)
 * Connect with public peers (such as pubs) to find friends.
   * Pubs require an invite (easy to get)
   * Pubs may go away some day (?)

---

### Database Persistence

 * Creates ~/.ssb directory:
    * configs
    * database / feeds
    * key pair (your identity)

---

### Significant Features

  * Log replication is possible over different channels (even sneakernet)
  * Interact with peers offline. Sync later when WiFi is available.
  * 100% decentralized - "No singletons" is a guiding constraint.
  * Replicated blob storage for file sharing.

---

 ### Limitations

  * Data is replicated: DBs are large. (~2.5GB)
  * Data is replicated: Can't unsay things.
  * Not good for blockchains (next slide)

---

### No Crypto-Currencies

From the docs:

 * eventually-consistent with peers
 * requires exterior coordination to create strictly-ordered transactions.
 * Probably a poor choice for a crypto-currency.

---

### Architecture

  * Protocol negotiates peer discovery, key transfer, transport security.
  * Users have 1+ identities (keypair + time series database)
  * Users replicate a copy of friends "feed"
    * Hashed / Signed by sender, preventing tampering.
    * Hash chain (no forks) of ordered JSON objects.

---

### Peer Discovery

 * Once you have an identity, you can interact with peers.
 * LAN peer discovery is broadcast via UDP
 * Sync with peers to get new messages (or not).
 * Public discovery happens via "pubs", a node with a public IP.
 * Pubs are not servers

---

### `sbot`, an SSB CLI tool

---

### "Hello" With SBot

 > sbot publish --type post --text "Hello"

```javascript
{
  "key": "%j5ivAhi2...iwSwO0ZBy0=.sha256", // For `ssb://` URLs and such
  "value": { "see_next_slide": "see_next_slide" },
  "timestamp": 1544419961590.001
}
```

---

```javascript
  {
      "previous": "%FRb...L8OU=.sha256",
      "sequence": 74,
      "author": "@uI150...DOcuQA=.ed25519",
      "timestamp": 1544419961590,
      "hash": "sha256",
      "content": { "type": "post", "text": "Hello" },
      "signature": "qJl0...uICr2Pl0DaVRnyZDw==.sig.ed25519"
    }
```
---

### Ecosystem, Notable Projects

  * [Scuttlebot](https://github.com/ssbc/scuttlebot)
  * [ssb-exporter](https://github.com/arj03/ssb-exporter)
  * [Pub server list](https://github.com/ssbc/scuttlebot/wiki/Pub-Servers)
  * [Dark Crystal](https://darkcrystal.pw/)
  * [Decent](https://github.com/evbogue/decent)

---

### Dark Crystal

 * https://darkcrystal.pw/img/screencast.gif

---

### Special Thanks + Dark Crystal Q&A

 * Mix, Everett, Gwen for help and feedback.
 * All of the SSB / Dark Crystal core team members for showing up.
