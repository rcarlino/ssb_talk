 * Any big changes coming to the protocol in 2019?
 * What's the biggest challenge for the project in 2019?
 * Any cool SSB use cases outside of Patchwork you'd like to mention? Specifically, offline use cases? (my personal interest in SSB).
 * Are any alternative implementations of SBot (for other languages) emerging?
 * I still don't understand the 4-step secret handshake protocol. How does it work in practice for real-world apps like Patchwork?
 * How does dark crystal work?
 * Anyone using dark crystal yet?
 * Most SSB things I've seen gossip over HTTP (port 8008) or WS (port 8989). Are there other ways to get updates?
 * Some users have noted large .ssb directories. Can feeds be compacted or compressed over time?
 * what is scope? Like the scope used in Sbots getaddress
 * What are these voting messages in my DB all about?
 * How does gossip and replication work at the network / storage layer?
    * EG: How does data move from my DB to a peers DB?
