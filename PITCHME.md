### Secure Scuttlebutt Protocol

Replicated,  P2P, "off grid" log database

---

### Outline

 * Introduce Pathwork, an SSB application.
 * Protocol's architecture overview
 * Hand it over to SSB devs.

---

### Real SSB Use Case: Patchwork

 * SSB is a peer-to-peer database.
 * Applications utilize Databases.
 * Patchwork is a P2P social messenger, powered by SSB (Twitter-ish)

---

### How Patchwork Works

 * When you follow a user, SSB will:
   * Replicate logs of that friend
   * Replicate that user's friends, also (configurable)
 * Once connected you can
   * send end-to-end private messages
   * Change clients (patchbay, patchfoo, patchwork)
   * Block users (stop replicating)
   * Name users ("pet names")

---

### How SSB Makes Patchwork Different

  * End-to-end encryption via "Secret Handshake"
  * Internet not required, even for posting messages
  * Offline first, sync later replication

---

### So What is SSB Then?

  * The protocol / database powering Patchwork
  * Usually used for offline-first desktop apps.
    * Not exclusively a social app technology (SEE: `ssb-chess`).
    * Works well for social apps, though.

---

### Key Concepts, I

  * Database entries (JSON) replicated among peers
  * Protocol negotiates peer discovery, key transfer, transport security.
  * Database entries form an immutable hash-chain.
  * No centralized authorities, ever (Later: Dark Crystal)
    * Avoid singletons (Servers, DHTs, Global blockchains)

---

### Key Concepts, II

  * Entries hashed / signed by sender, preventing tampering.
  * "Gossip" and "Listening Distance" - Hops between peers
  * Users have 1+ identities (keypair + time series database)

---

### Features

  * Log replication is possible over different channels (even sneakernet)
  * Strong offline-first support.
  * Supports replicated blobs for file sharing.

---

 ### Limitations

  * Data is replicated: DBs are large. (~2.5GB)
  * Data is replicated: Can't unsay things.
  * Not good for blockchains (next slide)

---

### No Crypto-Currencies

From the docs:

 * eventually-consistent with peers
 * requires exterior coordination to create strictly-ordered transactions.
 * Probably a poor choice for a crypto-currency.
