
I recommend you show people Patchwork before talking about it (from experience this help people a lot)
modify : > You need to follow someone using a "pub"
pubs are always online peers which act a lot like your common watering hole - you know where they are in your community and you can swing by to chat with the person behind the bar to ask about the current 'gossip' for friends of yours.
these are one of the hardest concepts to explain. In the long term we plan for them not to exist :)
key concept:
Subjectivity - this is an important corollary of no global singleton + free listening. Because you only see those you follow, and the database is open, there's no single way to interpret or "view" the gossip. e.g. just because you can rename people in Patchwork, doesn't mean the client Ticktack (a blogging client) will display it or allow you to do that.
notable projects - see : https://www.scuttlebutt.nz/applications , it has screenshots!
