### What is This?

I plan on giving this talk to a non-technical audience at a later time.

The slides below are the start of a non-technical intro.

### Part I: What Makes Software "Decentralized"?

A high-level overview for non-technical or unfamiliar audiences

---

### What Are We Talking About?

  * Decentralized software, especially Secure Scuttlebutt (more later).
  * But what does decentralized mean?

---

### Four Software Models

  * Centralized
  * Decentralized:
    * Self-hosted
    * Federated
    * Peer-to-peer
  * Other paradigms may exist, but are irrelevant to today's discussion.

---

### What Does "Centralized" Mean?

  * Your data (and often, the application) live in one place.
  * Application does not work without a central point (usually a server).
  * Centralized applications may implement load-balancing, but it is still centralized if users can only access the application in one place.

---

### Centralized Software Examples

  * "Platforms" like Facebook, Twitter, QuickBooks, Dropbox
  * Most App Stores
  * In the 90's: Services like AOL, Compuserve, Prodigy
  * "Walled Gardens"

---

### Centralized Software Benefits

  * Easy to manage
  * Easy to use
  * Easy to monetize

---

### Centralized Software Tradeoffs

  * Lack of user control
  * Privacy invasion
  * Social issues, such as manipulation and filter bubbles ("algorithmic feeds")
  * Censorship
  * Surveillance

---

### What Does "Self-hosted" Mean?

  * It's centralized software that runs on _your hardware_.

---

### Self-hosted Software Examples

  * Wordpress
  * Most database packages (excluding proprietary cloud-based ones)
  * Network attached storage servers

---

### Self-hosted Software Benefits

  * Simplicity of centralized software.
  * More control over how your data is handled

---

### Self-hosted Software Tradeoffs

  * Setup requires technical expertise.
  * By running self-hosted software, you become a sysadmin.

---

### What Does "Federated" Mean?

  * Many servers intercommunicating.
  * Clients are free to move between servers

---

### Federated Software Examples

  * Email providers
  * Mastodon
  * HTTP (excluding "platforms")
  * IRC Chat
  * Older tech: `gopher://`, `ftp://`, etc..

---

### Federated Software Benefits

  * Offers data control comparable to self-hosted software
  * Interoperability
    * GMail users can talk to Yahoo Mail users
  * Multitude of free and paid providers
    * Don't like your vendor? Take your money elsewhere.
    * Want better privacy than GMail? Switch to ProtonMail, etc etc..
  * Improved fault tolerance
    * GMail outages don't affect Yahoo Mail

---

### Federated Software Tradeoffs

  * All participants must agree on standards
    * Leads to slow development cycles
    * Leads to "death by committee"
    * Leads to "Internet Explorer Syndrome"
      * One key player takes majority stake and subverts standards
  * Very few federated protocols have seen adoption since 00's
  * Federated services can become centralized via monopolies

---

### What Does "Peer-to-Peer" Mean?

  * Every participant in the network is equal.
  * Client and server are wrapped into one package.

---

### Peer-to-Peer Software Examples

  * Napster, Gnutella, Bittorent
  * Bitcoin, blockchains
  * Freenet, IPFS, Web alternatives

---

### Peer-to-Peer Software Benefits

  * Fault tolerance
  * Censorship resistance
  * User control

---

### Peer-to-Peer Software Tradeoffs

  * Networking and peer discovery
  * Technical overhead

---
